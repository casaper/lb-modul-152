"use strict";

/**
 * Utility Funktionen fuer Wurmpicker Spiel
 *
 * @author: Marc Jenni, Kaspar Vollenweider
 * @date: 9.1.2018
 * @project: LB ZLI Modul 152
 */

/**
 * Utility um stroke() und fill() in einem aus zu führen
 * @param {*} ctx - Canvas graphic context
 * @return void
 */
var strokeAndFill = function strokeAndFill(ctx) {
  ctx.stroke();
  ctx.fill();
};

/**
 * Löscht ganzen Canvas Inhalt
 * @param {*} ctx - Canvas graphic context
 * @param {*} canvas - Canvas Object
 * @return void
 */
var clearCanvas = function clearCanvas(ctx, canvas) {
  return ctx.clearRect(0, 0, canvas.width, canvas.height);
};

/**
 * Ist eine Position in einer Flaeche
 * @param {*} x
 * @param {*} y
 * @param {*} area
 * Area ist ein Object bestehend aus:
 *  xa - links oben X-Koordinate
 *  xb - rechts unten X-Koordinate
 *  ya - links oben Y-Koordinate
 *  yb - rechts unten Y-Koordinate
 */
var isPositionInArea = function isPositionInArea(x, y, _ref) {
  var _ref$xa = _ref.xa,
      xa = _ref$xa === undefined ? 0 : _ref$xa,
      _ref$xb = _ref.xb,
      xb = _ref$xb === undefined ? 0 : _ref$xb,
      _ref$ya = _ref.ya,
      ya = _ref$ya === undefined ? 0 : _ref$ya,
      _ref$yb = _ref.yb,
      yb = _ref$yb === undefined ? 0 : _ref$yb;
  return x > img.xa && y > img.ya && x < img.xb && y < img.yb;
};

/**
 * Bild in Canvas zeichnen
 * @param {*} src - die URI zum bild
 * @param {*} ctx - Canvas graphic context
 * @param {*} x - X-Koordinate in Canvas
 * @param {*} y - Y-Koordinate in Canvas
 * @param {*} width - Breite des Bildes (Darstellung, nicht original)
 * @param {*} height - Höhe des Bildes (Darstellung, nicht original)
 */
var drawImage = function drawImage(src, ctx) {
  var x = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var y = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
  var width = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
  var height = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

  var image = new Image();
  image.src = src;
  image.onload = function () {
    ctx.drawImage(image, 0, 0, image.naturalWidth, image.naturalHeight, x, y, width || image.naturalWidth, height || image.naturalHeight);
  };
  return src;
};

var getViewport = function getViewport() {
  return {
    width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
    height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
  };
};

/**
 * Document loaded event helper
 * @param {*} callback - auszuführen on document loaded
 */
var onLoaded = function onLoaded(callback) {
  return document.addEventListener("DOMContentLoaded", callback);
};
//# sourceMappingURL=utilities.js.map
