'use strict';

/**
 * Main Scripts fuer Wurmpicker Spiel
 *
 * @author: Marc Jenni, Kaspar Vollenweider
 * @date: 9.1.2018
 * @project: LB ZLI Modul 152
 */

onLoaded(function () {
  // Get canvas and Graphics Context
  var canvas = document.getElementById('huenerhof');
  var ctx = canvas.getContext('2d');

  // Browser viewport Breite und Höhe
  var viewport = getViewport();

  // Einrichtung der Stage.
  var stage = {
    width: Math.max(viewport.width * 0.8), // Breite 80% des Viewports
    height: Math.max(viewport.height * 0.8), // Hoehe 80% des Viewports
    canvas: canvas

    // Setze Canvas Groesse
  };canvas.setAttribute('width', stage.width);
  canvas.setAttribute('height', stage.height);
});
//# sourceMappingURL=main.js.map
