var gulp = require('gulp'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    runSequence = require('run-sequence'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function() {
  return gulp.src('style/**/*.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./style'));
});

gulp.task('js', function() {
  gulp.src('js_es6/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env', 'es2016']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('js'))
});


gulp.task('default', [ 'js', 'sass' ]);

gulp.task('watch', function() {
  gulp.watch('js_es6/**/*.js', ['js']);
  gulp.watch('style/**/*.sass', ['sass']);
});



