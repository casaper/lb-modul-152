# Wurmpicker – Huhn pickt Würmer

## Auftrag

Wir möchten ein einfaches Hühnerhofspiel für kleine Kinder machen. Die Geschichte ist, dass ein Huhn im Hühnerhof bewegt werden kann, und irgendwo im Hühnerhof befindet sich ein Wurm. Wird das Huhn nun mit den Pfeiltasten auf den Wurm bewegt, pickt es diesen auf und gackert danach stolz. Der Wurmzähler neben dem Spielfeld wird um eins erhöht und es taucht an einer anderen Stelle wider ein neuer Wurm auf.

![Spielfeld Beispiel](spielfeld.jpg)

### Spielfeld

- grösse passt sich Viewport an
- quadratisch
- Bewegung im Spielfeld sind dynamisch

### Huhn

- Pickt Wurm wenn es den Wurm erreicht
  - Kopf geht vorne runter und wider hoch
- Bild eines Huhns von der Seite
  - 3 Bilder für die Animation der Kopfbewegung beim Gehen des Huhns
  - 1 Bild mit Kopf nach unten für die darstellung des Pickens
- gackert wenn es den Wurm gefressen hat
- wenn es läuft, bewegt sich der Kopf vor und Zurück (Hühnergang)
- steuerbar über Pfeiltasten
  - läuft wenn Pfeiltaste gedrückt wird
  - stoppt wenn Pfeiltaste nicht gedrückt wird
  - Pfeiltasten geben Richtung vor

### Wurm

- ist gut sichtbar aber etwas kleiner wie das Huhn
- zufällig im Spielfeld platziert
- wenn gefressen wird neuer erzeugt

### Spiellogik

- Counter mit Anzahl gefressenen Würmern