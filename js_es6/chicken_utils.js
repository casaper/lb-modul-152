

/**
 * Position des naechsten Schritts des Huhns bestimme
 * @param {*} stage
 * @return chicken - Objekt; chicken state
 */
const chickenMoveStep = ({chicken}) => {
  const { x, y, direction, stepSize, width, height, currentImg, images} = chicken;
  chicken.currentImg += 1;
  if(chicken.currentImg > images.length - 1) {
    chicken.currentImg = 0;
  }
  if(direction == 'up') {
    chicken.y -= stepSize;
    return chicken;
  }
  if(direction == 'right') {
    chicken.x += stepSize;
    return chicken;
  }
  if(direction == 'down') {
    chicken.y += stepSize;
    return chicken;
  }
  if(direction == 'left') {
    chicken.x -= stepSize;
    return chicken;
  }
}

/**
 * Abstand zum Zaun (in Bewegungsrichtung)
 * @param {*} stage
 * @return integer - Abstand zum Zaun
 */
const chickenToFence = (stage) => {
  const {chicken: {direction, x, y, width, height}} = stage;
  if(direction == 'up') {
    return Math.floor((-1 * stage.height) + y + stage.height);
  }
  if(direction == 'right') {
    return Math.floor(stage.width - (x + width));
  }
  if(direction == 'down') {
    return Math.floor(stage.height - (y + height));
  }
  if(direction == 'left') {
    return Math.floor((-1 * stage.width) + x + stage.width);
  }
}

/**
 * Erstmalige Platzierung des Huhns im Spielfeld
 * @param {*} stage - Spiel states
 * @param {*} params - Default parameter fuer Chicken (wenn keine Konfiguration vorhanden)
 * @return stage - Spiel states
 */
const placeChicken = (stage, { chicken: {
  width = 50, height = 50, currentImg = 0, stepSize,
  images = ['step1.jpg', 'step2.jpg', 'step3.jpg']}}
) => {
  // Den chicken state initialisieren
  const chicken = {
    // Initial Position in der Mitte des Huenerhofs
    x: Math.ceil((stage.width/2) - width/2),
    y: Math.ceil((stage.height/2) - height/2),
    width,      // Breite des Huhns
    height,     // Hoehe des Huhns
    images,     // Bilder der Verschiedenen Gehzuestaende des Huhns
    currentImg, // Das aktuell darzustellende Huenerbild
    stepSize    // Groesse eines Schrittes
  }
  stage.chicken = chicken;  // Huhn Spiel-Objekt zuweisen
  drawChicken(stage);       // Initiales Zeichnen des Huhns
  return stage;
}

/**
 * Das Huhn ins Canvas zeichnen
 * @param {*} stage
 */
const drawChicken = ({ctx, chicken: {x, y, width, height, images, currentImg, stepSize}}) => {
  drawImage(`img/chicken/${images[currentImg]}`, ctx, x, y, width, height);
}
