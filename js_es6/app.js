var xPlayer = 80;
var yPlayer = 80;
var fps = 30;
var canvas = null;
var ctx = null;
var imgPlayer = null;

window.onload = function () {
    initializeGame();

    setInterval(function () {
        update();
    }, 1000 / fps);
}

function initializeGame() {
    canvas = document.getElementById("gameCanvas");
    ctx = canvas.getContext("2d");

    ctx.canvas.width = 500;
    ctx.canvas.height = 500;

    imgPlayer = new Image();
    imgPlayer.src = "assets/images/dot.png";

    imgPlayer.onload = function () {
        ctx.drawImage(imgPlayer, 50, 60);
    }
}

function update() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.fillStyle = "#92B901";
    ctx.fillRect(50, 50, 500, 500);

    document.addEventListener('keydown', function (event) {
        if (event.keyCode == 37) {
            ctx.drawImage(imgPlayer, 50, 100);
        }

        if (event.keyCode == 39) {
            alert('Right was pressed');
        }

        if (event.keyCode == 40) {
            alert('Down was pressed');
        }

        if (event.keyCode == 38) {
            alert('Up was pressed');
        }
    });

    ctx.drawImage(imgPlayer, 80, 80);
}