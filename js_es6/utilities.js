/**
 * Utility Funktionen fuer Wurmpicker Spiel
 *
 * @author: Marc Jenni, Kaspar Vollenweider
 * @date: 9.1.2018
 * @project: LB ZLI Modul 152
 */

/**
 * Utility um stroke() und fill() in einem aus zu führen
 * @param {*} ctx - Canvas graphic context
 * @return void
 */
const strokeAndFill = (ctx) => {
  ctx.stroke();
  ctx.fill();
}

/**
 * Löscht ganzen oder Teil des Canvas-Inhalts
 * @param {*} stage - Applikations container mit canvas und Graphic context
 * @param partial - Optional; Flaeche inerhalb des Canvas
 * @return stage
 */
const clearCanvas = (stage, partial = null) => {
  if(partial) { // Loesche nur bestimmte Canvas region
    stage.ctx.clearRect(partial.x, partial.y, partial.width, partial.height);
  } else { // Loesche ganzes Canvas
    stage.ctx.clearRect(0, 0, stage.canvas.width, stage.canvas.height);
  }
  return stage;
}

/**
 * Ist eine Position in einer Flaeche
 * @param {*} x
 * @param {*} y
 * @param {*} area
 * Area ist ein Object bestehend aus:
 *  xa - links oben X-Koordinate
 *  xb - rechts unten X-Koordinate
 *  ya - links oben Y-Koordinate
 *  yb - rechts unten Y-Koordinate
 * @return boolean
 */
const isPositionInArea = (x, y, {xa = 0, xb = 0, ya = 0, yb = 0}) => (
  x > img.xa && y > img.ya && x < img.xb && y < img.yb
);

/**
 * Bild in Canvas zeichnen
 * @param {*} src    - die URI zum bild
 * @param {*} ctx    - Canvas graphic context
 * @param {*} x      - X-Koordinate in Canvas
 * @param {*} y      - Y-Koordinate in Canvas
 * @param {*} width  - Breite des Bildes (Darstellung, nicht original)
 * @param {*} height - Höhe des Bildes (Darstellung, nicht original)
 * @return Canvas 2d graphic context
 */
const drawImage = (src, ctx, x = 0, y = 0, width = null, height = null) => {
  const image = new Image();
  image.src = src;
  image.onload = () => {
    clearCanvas({ctx}, {x: x - 5, y: y - 5, width: width + 9, height: height + 9})
    ctx.drawImage(
      image,                        // Zu zeichnendes Bild
      0,                            // Source anfang x
      0,                            // Source anfang y
      image.naturalWidth,           // Source Breite
      image.naturalHeight,          // Source Hoehe
      x,                            // x im Canvas
      y,                            // y im Canvas
      width || image.naturalWidth,  // Breite oder Breite des Originalbilds
      height || image.naturalHeight // Hoehe oder Hoehe des Originalbilds
    );
  }
  return ctx;
}

/**
 * Hoehe und Breite des Browserfensters ermitteln
 * @return vieport Groesse
 */
const getViewport = () => (
  {
    width: Math.floor(document.documentElement.clientWidth, window.innerWidth || 0),
    height: Math.floor(document.documentElement.clientHeight, window.innerHeight || 0)
  }
);
