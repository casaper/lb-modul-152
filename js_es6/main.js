/**
 * Main Scripts fuer Wurmpicker Spiel
 *
 * @author: Marc Jenni, Kaspar Vollenweider
 * @date: 9.1.2018
 * @project: LB ZLI Modul 152
 */

// Haupt Spiel Objekt das alle relevanten States enthaelt
let stage;

// Haupt konfiguration des Spiels
const params = {
  chicken: {
    width: 50,
    height: 50,
    images: ['step1.jpg', 'step2.jpg', 'step3.jpg'],
    stepSize: 5
  },
  directionKeys: { 37: 'left', 38: 'up', 39: 'right', 40: 'down' }
}

/**
 * Programm initialisierung (onLoad)
 */
window.onload = () => {
  stage = setUpStage(params);
  stage = addArrowKeyListener(stage, params);
}

/**
 * Fuegt Listener fuer Pfeiltasten hinzu,
 * @param {*} stage - Spiel objekt
 * @param {*} params - Konfigurationsparameter
 * @return stage - spiel states
 */
const addArrowKeyListener = (stage, params) => {
  document.addEventListener('keydown', ({which}) => {
    // Wenn pfeiltaste gedruekt wird, Huhn bewegen
    if(params.directionKeys[which] && stage.chicken.direction !== params.directionKeys[which]) {
      stage = abortChickenMove(stage);
      stage.chicken.direction = params.directionKeys[which];
      stage = triggerChickenMoove(stage, params, params.directionKeys[which]);
    }
  });
  return stage;
}

/**
 * Huhn bewegungs Animation anwerfen
 * @param {*} stage - Spiel objekt
 * @param {*} params - Konfigurationsparameter
 * @param {*} direction - Fortbewegungs richtung des Huhns
 * @return stage - spiel states
 */
const triggerChickenMoove = (stage, params, direction) => {
  // Wenn Huhn am Zaun steht soll es nicht weiter in richtung Zaun gehen koennen
  if(chickenToFence(stage) < 1) {
    return stage;
  }
  stage.chicken.move = window.requestAnimationFrame(timestamp => {
    moveChicken(timestamp, stage);
  });

  // Beenden der Fortbewegung wenn Pfeiltaste keyup oder andere Pfeiltaste
  stage.chicken.moveEndListener = document.addEventListener('keyup', ({which}) => {
    if(params.directionKeys[which] && stage.chicken.direction === params.directionKeys[which]) {
      stage = abortChickenMove(stage);
    }
  });

  return stage;
}

/**
 * Rekursives Callback fuer Huener Lauf Animation
 * @param {*} timestamp - Canvas animations timestamp
 * @param {*} stage - Spiel objekt
 * @return void
 */
const moveChicken = (timestamp, stage) => {
  stage.chicken = chickenMoveStep(stage);
  drawChicken(stage);
  if(chickenToFence(stage) > 0) {
    stage.chicken.move = window.requestAnimationFrame((timestamp) => {
      moveChicken(timestamp, stage);
    });
  }
}

/**
 * Huhn Stoppen
 * @param {*} stage - Spiel objekt
 * @return stage - Spiel states
 */
const abortChickenMove = (stage) => {
  window.cancelAnimationFrame(stage.chicken.move);
  stage.chicken.moove = null;
  stage.chicken.direction = null;
  return stage;
}

/**
 * Stage aufbau und konfiguration
 * @return stage - Ein objekt das alle wichtigen objekte und Werte zum
 *                 Spiel enthaelt
 */
const setUpStage = (params) => {
  // Browser viewport Breite und Höhe
  const viewport = getViewport();
  // Get canvas and Graphics Context
  const canvas = document.getElementById('huenerhof');
  const ctx = canvas.getContext('2d');

  const stage = {
    width: Math.max(viewport.width * 0.8),  // Breite 80% des Viewports
    height: Math.max(viewport.height * 0.8), // Hoehe 80% des Viewports
    // Weitere Objekte und eigenschaften zu Stage
    canvas,  // Canvas main stage
    ctx,     // Canvas 2d graphic context
    viewport // Browser viewport groesse
  }

  // Setze Canvas Groesse
  canvas.setAttribute('width', stage.width);
  canvas.setAttribute('height', stage.height);

  // Speichere Canvas-Groesse, position etc. in stage
  stage.rect = stage.canvas.getBoundingClientRect();

  // Stage Object fertig initialisiert, Chicken plazieren
  return placeChicken(stage, params);
}
